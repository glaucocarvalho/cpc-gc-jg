package controle;

import javax.swing.JTextField;

import java.lang.*;  // TO compare strings

import modelo.*;
import rede.*;

public class Fluxo extends Thread {

	// Project OBJ //
	Modelo M = new Modelo();
	
	// JTextFields //
	JTextField textoMsgRec;
	JTextField textoMsgEnv;
	JTextField textoEstadoAtual;
	String msg = "nehuma";
	String lastEstado = "";
	
	// Getters and setters //
	public JTextField getTextoMsgRec() {
		return textoMsgRec;
	}

	public void setTextoMsgRec(JTextField textoMsgRec) {
		this.textoMsgRec = textoMsgRec;
	}

	public JTextField getTextoMsgEnv() {
		return textoMsgEnv;
	}

	public void setTextoMsgEnv(JTextField textoMsgEnv) {
		this.textoMsgEnv = textoMsgEnv;
	}

	public JTextField getTextoEstadoAtual() {
		return textoEstadoAtual;
	}

	public void setTextoEstadoAtual(JTextField textoEstadoAtual) {
		this.textoEstadoAtual = textoEstadoAtual;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getLastEstado() {
		return lastEstado;
	}

	public void setLastEstado(String lastEstado) {
		this.lastEstado = lastEstado;
	}
	
	// Aux Funcitons //
	// TODO: completar fun��o
	public void setPecaPronta() {
		M.setPecaPronta(true);
	}
	
	public String getPecaPronta() {
		String ret = "";
		if (M.isPecaPronta()){
			ret = "Peca Pronta";
		}
		if (!M.isPecaPronta()){
			ret = "SEM Peca Pronta";
		}
		return ret;
	}

	// "main" //
	public void acionar ()
	{		
		boolean fim = false;
		
		// server conf //
		Servidor server = new Servidor();   // Meu endere�o e porta		
	    server.start();						// meuServidor.start

	    // Addr and port to the server i want to connect to //
		// aqui vou precisar de 3 clientes, um pra cada esteira e um pro elevador.
		// por hora vai ficar assim
	    // pensamento: talvez 3 fluxos? esteiraA-Carrinho|esteiraB-Carrinho|elevador-Carrinho
		Cliente cliente = new Cliente();
		cliente.setIPhost("localhost");

		// Isto � apenas um lembrete para as portas //
	    // carrinho.porta = 1111
	    // esteiraA.porta = 2222
	    // esteiraB.porta = 3333
	    // elevador.porta = 4444
		
		cliente.setPortaServidor(2222);
	    boolean conectouCarrinho = false;
//		cliente.setPortaServidor(3333);
//	    boolean conectouEsteiraB = false;
//		cliente.setPortaServidor(4444);
//	    boolean conectouElevador = false;
	    
	    while (!conectouCarrinho && !fim)
	    	{
	    	conectouCarrinho = cliente.conectarAoservidor();
	    	}
	    
	    // "main" //
		while (!fim)
		{
			M.executar();
			textoEstadoAtual.setText(M.getEstado().toString());

			// Se o estado mudou setLastEstado // 
			if (!M.getEstado().equals(lastEstado)){
				lastEstado = M.getEstado().toString();
				textoEstadoAtual.setText(lastEstado);
			}
			
			// Received messages and take actions //
			// DO NOT FORGET THE else Cx.setMensagem("nenhuma");
			if (msg.equals("LiberadaA") && M.getEstado().equals("EsperaCarrinho")){
				M.setMensagem("LiberadaA");
			}
			else M.setMensagem("nenhuma");
		}	
		// ??? //
		if (conectouCarrinho) 
		{
			cliente.encerraConexao();
			server.setListening(false);
		}
	}
	
	public void run ()
	{
		acionar();
	}	
}
