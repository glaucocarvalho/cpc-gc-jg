package visao;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JLabel;

import controle.*;

import java.awt.EventQueue;

public class Principal{

	private JFrame frmElevador;
	private JTextField campo_esteira;
	private JTextField textoRecebido;
	private JTextField textoEnviado;
	private JTextField msgEnv;
	private JTextField msgRec;
	private JTextField estadoAtual;
	private JTextField PecaProntaA;
	
	Fluxo fc = new Fluxo();

	/**
	 * Launch the application.
	 */
	
	//FluxoEsteira fe = new FluxoEsteira(); 
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frmElevador.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Principal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmElevador = new JFrame();
		frmElevador.setTitle("Esteira B");
		frmElevador.setBounds(100, 100, 510, 229);
		frmElevador.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmElevador.getContentPane().setLayout(null);
		
		msgEnv = new JTextField();
		msgEnv.setBounds(10, 45, 214, 20);
		frmElevador.getContentPane().add(msgEnv);
		msgEnv.setColumns(10);
		
		msgRec = new JTextField();
		msgRec.setBounds(10, 101, 214, 20);
		frmElevador.getContentPane().add(msgRec);
		msgRec.setColumns(10);
		
		estadoAtual = new JTextField();
		estadoAtual.setBounds(10, 157, 214, 20);
		frmElevador.getContentPane().add(estadoAtual);
		estadoAtual.setColumns(10);
		
		JLabel labelMsgEnv = new JLabel("Mensagem Enviada para o Carrinho");
		labelMsgEnv.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelMsgEnv.setBounds(10, 20, 214, 14);
		frmElevador.getContentPane().add(labelMsgEnv);
		
		JLabel labelMsgRec = new JLabel("Mensagem Recebida do Carrinho");
		labelMsgRec.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelMsgRec.setBounds(10, 76, 214, 14);
		frmElevador.getContentPane().add(labelMsgRec);
		
		JLabel labelEstadoAtual = new JLabel("Estado Atual");
		labelEstadoAtual.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelEstadoAtual.setBounds(10, 132, 185, 14);
		frmElevador.getContentPane().add(labelEstadoAtual);
		
		JButton jButtonIniciar = new JButton("Iniciar Servidor");
		jButtonIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				msgEnv.setText("");
				fc.setTextoMsgEnv(msgEnv);
				fc.setTextoMsgRec(msgRec);
				fc.setTextoEstadoAtual(estadoAtual);
				fc.start();
				
			}
		});
		jButtonIniciar.setBounds(270, 44, 214, 23);
		frmElevador.getContentPane().add(jButtonIniciar);
		
		JButton jButtonSetPecaProntaB= new JButton("setPecaProntaB=True");
		jButtonSetPecaProntaB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				fc.setPecaPronta();			
				PecaProntaA.setText(fc.getPecaPronta());	
			}
		});
		jButtonSetPecaProntaB.setBounds(270, 89, 214, 23);
		frmElevador.getContentPane().add(jButtonSetPecaProntaB);
		
		PecaProntaA = new JTextField();
		PecaProntaA.setText(fc.getPecaPronta());
		PecaProntaA.setBounds(270, 157, 214, 20);
		frmElevador.getContentPane().add(PecaProntaA);
		PecaProntaA.setColumns(10);
		
		JLabel labelPecaProntaA = new JLabel("A pe\u00E7a est\u00E1 pronta?");
		labelPecaProntaA.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelPecaProntaA.setBounds(270, 132, 214, 14);
		frmElevador.getContentPane().add(labelPecaProntaA);
	}
}
