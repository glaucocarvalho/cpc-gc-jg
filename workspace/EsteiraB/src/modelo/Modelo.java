package modelo;

import javax.swing.JOptionPane;

import java.lang.*;  // TO compare strings
//import java.io.*;  // TO .split


public class Modelo {
	// this project model estados //
	public static enum tipoEstado {
		Transporte,
		EsperaCarrinho,
	};
	
	// Defaults //
	boolean transmitiu = false;
	boolean mostrei = false;
	tipoEstado estado = tipoEstado.Transporte;
	String mensagem = "nenhuma";
	String MyName = "Esteira B";	
	
	// Booleans //
	boolean pecaPronta = false;

	// Getters and setters //
	
	public boolean isTransmitiu() {
		return transmitiu;
	}

	public void setTransmitiu(boolean transmitiu) {
		this.transmitiu = transmitiu;
	}

	public boolean isMostrei() {
		return mostrei;
	}

	public void setMostrei(boolean mostrei) {
		this.mostrei = mostrei;
	}

	public tipoEstado getEstado() {
		return estado;
	}

	public void setEstado(tipoEstado estado) {
		this.estado = estado;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getMyName() {
		return MyName;
	}

	public boolean isPecaPronta() {
		return pecaPronta;
	}

	public void setPecaPronta(boolean estouVazio) {
		this.pecaPronta = estouVazio;
	}
	
	
	// Aux functions	
	public void transmite (String msg)
	{
		transmitiu = true;
		mensagem = msg;
	}	
	
	
	// "Main" //
	public void executar() {		
		  switch (estado)
		  {
		   	case Transporte:
		   		if (!mostrei) 
	   			{
		   			System.out.println("["+ MyName +"] - Estou no estado " + estado + ".");
		   			mostrei = true;
	   			}
		   		// o que ue fa�o em cada estado em rela��o ao pr�prio modelo.'
		   		if (pecaPronta){
		   			estado = tipoEstado.EsperaCarrinho;
		   			transmite("NovaPecaB");
			   		mostrei = false;
		   		}
		   		break;
		   	case EsperaCarrinho:
		   		if (!mostrei) 
	   			{
		   			System.out.println("["+ MyName +"] - Estou no estado " + estado + ".");
		   			mostrei = true;
	   			}
		   		if (mensagem.equals("LiberadaB")){
		   			estado = tipoEstado.Transporte;
			   		mostrei = false;
		   		}
		   		break;		   		
		  }
	}	
}
