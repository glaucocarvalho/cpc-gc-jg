package controle;

import javax.swing.JTextField;

import java.lang.*;  // TO compare strings

import modelo.*;
import modelo.Modelo.tipoEstado;
import rede.*;

public class Fluxo extends Thread {

	// Project OBJ //
	Modelo M = new Modelo();
	
	// JTextFields //
	JTextField textoMsgRec;
	JTextField textoMsgEnv;
	JTextField textoEstadoAtual;
	String msg = "nehuma";
	String lastEstado = "";
	
	// Getters and setters //
	public JTextField getTextoMsgRec() {
		return textoMsgRec;
	}

	public void setTextoMsgRec(JTextField textoMsgRec) {
		this.textoMsgRec = textoMsgRec;
	}

	public JTextField getTextoMsgEnv() {
		return textoMsgEnv;
	}

	public void setTextoMsgEnv(JTextField textoMsgEnv) {
		this.textoMsgEnv = textoMsgEnv;
	}

	public JTextField getTextoEstadoAtual() {
		return textoEstadoAtual;
	}

	public void setTextoEstadoAtual(JTextField textoEstadoAtual) {
		this.textoEstadoAtual = textoEstadoAtual;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getLastEstado() {
		return lastEstado;
	}

	public void setLastEstado(String lastEstado) {
		this.lastEstado = lastEstado;
	}
	
	// Aux Funcitons //
	// TODO: completar fun��o
	public void setPecaPronta() {
		M.setPecaPronta(true);
	}
	
	public String getPecaPronta() {
		String ret = "";
		if (M.isPecaPronta()){
			ret = "Peca Pronta";
		}
		if (!M.isPecaPronta()){
			ret = "SEM Peca Pronta";
		}
		return ret;
	}

	// "main" //
	public void acionar ()
	{		
		// constants //
		String serverIP = "localhost";
		int serverPort = 1111;		//Carrinho
		
		ClienteBC clienteBC = new ClienteBC();
	    boolean conectouCarrinho = false;		
	    clienteBC.setIPhost(serverIP);
	    clienteBC.setPortaServidor(serverPort);
	    String msgRecebida = "nenhuma";
	    String msgAnterior = "nenhuma";
	    
	    while (!conectouCarrinho)
	    	{
	    	conectouCarrinho = clienteBC.conectarAoservidor();
	    	}
	    clienteBC.start();
	    // "main" //
		while (conectouCarrinho)
		{
			M.executar();
//			Not necessary now
//			if (M.isEncerrarCaixa() == true)
//			{
//				clienteBC.encerraConexao();
//				conectouCarrinho = false;
//			}
			if (M.isTransmitiu()){
				clienteBC.enviarMensagem(M.getMensagem());
				System.out.println(M.getMensagem());
				M.setTransmitiu(false);
				M.setMensagem("nenhuma");
			}
			msgRecebida = clienteBC.getInputLine(); 
			if (!msgRecebida.equals(msgAnterior))
			{
				M.setMensagem(msgRecebida);
				System.out.println("mensagem: " + M.getMensagem());
				msgAnterior = msgRecebida;
			}
			try 
			{
				Thread.sleep(500);
			}
			catch (InterruptedException e) {}
		}	
		// ??? //
		if (conectouCarrinho) 
		{
			clienteBC.encerraConexao();
		}
	}
	
	public void run ()
	{
		acionar();
	}	
}
