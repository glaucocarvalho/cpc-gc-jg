package modelo;

import javax.swing.JOptionPane;

import java.lang.*;  // TO compare strings
//import java.io.*;  // TO .split


public class Modelo {
	// this project model estados //
	public static enum tipoEstado {
		NoAlto,
		NoBaixo,
	};
	
	// Defaults //
	boolean transmitiu = false;
	boolean mostrei = false;
	tipoEstado estado = tipoEstado.NoAlto;
	String mensagem = "nenhuma";
	String MyName = "Elevador";	
	
	// Booleans //
	boolean estouVazio = false;

	// Getters and setters //
	
	public boolean isTransmitiu() {
		return transmitiu;
	}

	public void setTransmitiu(boolean transmitiu) {
		this.transmitiu = transmitiu;
	}

	public boolean isMostrei() {
		return mostrei;
	}

	public void setMostrei(boolean mostrei) {
		this.mostrei = mostrei;
	}

	public tipoEstado getEstado() {
		return estado;
	}

	public void setEstado(tipoEstado estado) {
		this.estado = estado;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getMyName() {
		return MyName;
	}

	public boolean isEstouVazio() {
		return estouVazio;
	}

	public void setEstouVazio(boolean estouVazio) {
		this.estouVazio = estouVazio;
	}
	
	
	// Aux functions
	public void moverAlto ()
	{
		this.estado = tipoEstado.NoAlto;
	}	

	public void moverBaixo ()
	{
		this.estado = tipoEstado.NoBaixo;
	}	
	
	public void transmite (String msg)
	{
		transmitiu = true;
		mensagem = msg;
	}	
	
	
	// "Main" //
	public void executar() {		
		  switch (estado)
		  {
		   	case NoAlto:
		   		if (!mostrei) 
	   			{
		   			System.out.println("["+ MyName +"] - Estou no estado " + estado + ".");
		   			mostrei = true;
	   			}
		   		// o que ue fa�o em cada estado em rela��o ao pr�prio modelo.'
		   		if (mensagem.equals("JaEstaNoAlto")){
		   			transmite("Sim");
			   		mostrei = false;
		   		}
		   		if (mensagem.equals("Liberado")){
		   			moverBaixo();
			   		mostrei = false;
		   		}
		   		break;
		   	case NoBaixo:
		   		if (!mostrei) 
	   			{
		   			System.out.println("["+ MyName +"] - Estou no estado " + estado + ".");
		   			mostrei = true;
	   			}
		   		if (mensagem.equals("JaEstaNoAlto")){
		   			transmite("Nao");
			   		mostrei = false;
		   		}
		   		if (this.estouVazio){
		   			moverAlto();
		   		}
		   		break;		   		
		  }
	}	
}
