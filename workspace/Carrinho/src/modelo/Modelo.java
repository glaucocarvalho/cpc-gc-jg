package modelo;

import javax.swing.JOptionPane;
import java.lang.*;  // TO compare strings
//import java.io.*;  // TO .split


public class Modelo {
	// this project model estados //
	static enum tipoEstado {
		Repouso,
		SobreEsteiraA,
		SobreEsteiraB,
		SobreElevador,
	};
	
	// Defaults //
	boolean transmitiu = false;
	boolean mostrei = false;
	tipoEstado estado = tipoEstado.Repouso;
	String mensagem = "nenhuma";
	String MyName = "Carrinho";	
	
	// Booleans //
	boolean pegouPecaA = false;
	boolean pegouPecaB = false;
	
	// Getters and setters //
	public String getEstado() {
		return estado.toString();
	}
	public void setEstado(tipoEstado estado) {
		this.estado = estado;
	}
	public boolean isTransmitiu() {
		return transmitiu;
	}
	public void setTransmitiu(boolean transmitiu) {
		this.transmitiu = transmitiu;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}	
	public boolean isPegouPecaA() {
		return pegouPecaA;
	}
	public void setPegouPecaA(boolean pegouPecaA) {
		this.pegouPecaA = pegouPecaA;
	}
	public boolean isPegouPecaB() {
		return pegouPecaB;
	}
	public void setPegouPecaB(boolean pegouPecaB) {
		this.pegouPecaB = pegouPecaB;
	}
	
	// Aux functions
	public void transmite (String msg)
	{
		transmitiu = true;
		mensagem = msg;
	}
	
	private void moverParaEsteiraA(){
		estado = tipoEstado.SobreEsteiraA;
	}
	
	private void moverParaEsteiraB(){
		estado = tipoEstado.SobreEsteiraB;		
	}
	
	private void moverParaElevador(){
		estado = tipoEstado.SobreElevador;		
	}
	
	private void liberarPecaNoElevador(){
		// IDK what to do here
		int x = 1;
	}
	
	private void timeout(){
		// IDK what to do here
		int x = 1;		
	}
	
	// "Main" //
	public void executar() {		
		  switch (estado)
		  {
		   	case Repouso:
		   		if (!mostrei) 
	   			{
		   			System.out.println("["+ MyName +"] - Estou no estado " + estado + ".");
		   			mostrei = true;
	   			}
		   		// o que ue fa�o em cada estado em rela��o ao pr�prio modelo.'
		   		if (mensagem.equals("NovaPecaA")){
		   			moverParaEsteiraA();
			   		mostrei = false;
		   		}
		   		if (mensagem.equals("NovaPecaB")){
		   			moverParaEsteiraB();
			   		mostrei = false;
		   		}
		   		break;	
		   	case SobreEsteiraA:
		   		if (!mostrei) 
	   			{
		   			System.out.println("["+ MyName +"] - Estou no estado " + estado + ".");
		   			mostrei = true;
	   			}
		   		// o que ue fa�o em cada estado em rela��o ao pr�prio modelo.'
		   		if (pegouPecaA){
		   			transmite("LiberadaA");
		   			moverParaElevador();
			   		mostrei = false;
		   		}
		   		break;	
		   	case SobreEsteiraB:
		   		if (!mostrei) 
	   			{
		   			System.out.println("["+ MyName +"] - Estou no estado " + estado + ".");
		   			mostrei = true;
	   			}
		   		// o que ue fa�o em cada estado em rela��o ao pr�prio modelo.'
		   		if (pegouPecaB){
		   			transmite("LiberadaB");
		   			moverParaElevador();
			   		mostrei = false;
		   		}
		   		break;	
		   	case SobreElevador:
		   		if (!mostrei) 
	   			{
		   			System.out.println("["+ MyName +"] - Estou no estado " + estado + ".");
		   			mostrei = true;
	   			}
		   		// o que ue fa�o em cada estado em rela��o ao pr�prio modelo.'
	   			transmite("JaEstaNoAlto");

		   		if (mensagem.equals("Sim")){
		   			liberarPecaNoElevador();
		   			transmite("Liberado");
		   			estado = tipoEstado.Repouso;
			   		mostrei = false;
		   		}
		   		if (mensagem.equals("Nao")){
		   			timeout();
			   		mostrei = false;
		   		}
		   		break;
		  }
	}	
}
