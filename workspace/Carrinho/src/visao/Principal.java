package visao;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JLabel;

import controle.*;

import java.awt.EventQueue;

public class Principal{

	private JFrame frmCarrinho;
	private JTextField campo_esteira;
	private JTextField textoRecebido;
	private JTextField textoEnviado;
	private JTextField msgEnvA;
	private JTextField msgRecA;
	private JTextField estadoAtual;
	
	Fluxo fc = new Fluxo();
	private JTextField msgRecB;
	private JTextField msgEnvB;
	private JTextField msgRecE;
	private JTextField msgEnvE;
	private JTextField PegouPecaA;
	private JTextField PegouPecaB;

	/**
	 * Launch the application.
	 */
	
	//FluxoEsteira fe = new FluxoEsteira(); 
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frmCarrinho.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Principal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCarrinho = new JFrame();
		frmCarrinho.setTitle("Carrinho");
		frmCarrinho.setBounds(100, 100, 510, 414);
		frmCarrinho.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCarrinho.getContentPane().setLayout(null);
		
		msgEnvA = new JTextField();
		msgEnvA.setBounds(10, 45, 214, 20);
		frmCarrinho.getContentPane().add(msgEnvA);
		msgEnvA.setColumns(10);
		
		msgRecA = new JTextField();
		msgRecA.setBounds(10, 101, 214, 20);
		frmCarrinho.getContentPane().add(msgRecA);
		msgRecA.setColumns(10);
		
		estadoAtual = new JTextField();
		estadoAtual.setBounds(257, 331, 214, 20);
		frmCarrinho.getContentPane().add(estadoAtual);
		estadoAtual.setColumns(10);
		
		JLabel labelMsgEnvA = new JLabel("Mensagem Enviada para a EsteiraA");
		labelMsgEnvA.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelMsgEnvA.setBounds(10, 20, 214, 14);
		frmCarrinho.getContentPane().add(labelMsgEnvA);
		
		JLabel labelMsgRecA = new JLabel("Mensagem Recebida da EsteiraA");
		labelMsgRecA.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelMsgRecA.setBounds(10, 76, 214, 14);
		frmCarrinho.getContentPane().add(labelMsgRecA);
		
		JLabel labelEstadoAtual = new JLabel("Estado Atual");
		labelEstadoAtual.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelEstadoAtual.setBounds(257, 306, 185, 14);
		frmCarrinho.getContentPane().add(labelEstadoAtual);
		
		JButton jButtonIniciar = new JButton("Iniciar Servidor");
		jButtonIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				msgEnvA.setText("");
				fc.setTextoMsgEnv(msgEnvA);
				fc.setTextoMsgRec(msgRecA);
				fc.setTextoEstadoAtual(estadoAtual);
				fc.start();
				
			}
		});
		jButtonIniciar.setBounds(286, 44, 139, 23);
		frmCarrinho.getContentPane().add(jButtonIniciar);
		
		msgRecB = new JTextField();
		msgRecB.setColumns(10);
		msgRecB.setBounds(10, 213, 214, 20);
		frmCarrinho.getContentPane().add(msgRecB);
		
		JLabel labelMsgRecB = new JLabel("Mensagem Recebida da EsteiraB");
		labelMsgRecB.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelMsgRecB.setBounds(10, 188, 214, 14);
		frmCarrinho.getContentPane().add(labelMsgRecB);
		
		msgEnvB = new JTextField();
		msgEnvB.setColumns(10);
		msgEnvB.setBounds(10, 157, 214, 20);
		frmCarrinho.getContentPane().add(msgEnvB);
		
		JLabel labelMsgEnvB = new JLabel("Mensagem Enviada para a EsteiraB");
		labelMsgEnvB.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelMsgEnvB.setBounds(10, 132, 214, 14);
		frmCarrinho.getContentPane().add(labelMsgEnvB);
		
		msgRecE = new JTextField();
		msgRecE.setColumns(10);
		msgRecE.setBounds(10, 331, 214, 20);
		frmCarrinho.getContentPane().add(msgRecE);
		
		JLabel labelMsgRecE = new JLabel("Mensagem Recebida do Elevador");
		labelMsgRecE.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelMsgRecE.setBounds(10, 306, 214, 14);
		frmCarrinho.getContentPane().add(labelMsgRecE);
		
		msgEnvE = new JTextField();
		msgEnvE.setColumns(10);
		msgEnvE.setBounds(10, 275, 214, 20);
		frmCarrinho.getContentPane().add(msgEnvE);
		
		JLabel labelMsgEnvE = new JLabel("Mensagem Enviada para o Elevador");
		labelMsgEnvE.setFont(new Font("Tahoma", Font.PLAIN, 12));
		labelMsgEnvE.setBounds(10, 250, 214, 14);
		frmCarrinho.getContentPane().add(labelMsgEnvE);
		
		JButton PegouPegaB = new JButton("setPegouPegaB=True");
		PegouPegaB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				fc.setPegouPecaB(true);			
				PegouPecaB.setText(fc.getPegouPecaB());	
			}
		});
		PegouPegaB.setBounds(286, 154, 139, 23);
		frmCarrinho.getContentPane().add(PegouPegaB);
		
		JButton PegouPegaA = new JButton("setPegouPegaA=True");
		PegouPegaA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				fc.setPegouPecaA(true);			
				PegouPecaA.setText(fc.getPegouPecaA());	
			}
		});
		PegouPegaA.setBounds(286, 98, 139, 23);
		frmCarrinho.getContentPane().add(PegouPegaA);
		
		PegouPecaA = new JTextField();
		PegouPecaA.setText(fc.getPegouPecaA());
		PegouPecaA.setBounds(257, 213, 214, 20);
		frmCarrinho.getContentPane().add(PegouPecaA);
		
		JLabel lblPegouPecaA = new JLabel("Pegou pe\u00E7a A?");
		lblPegouPecaA.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPegouPecaA.setBounds(257, 188, 185, 14);
		frmCarrinho.getContentPane().add(lblPegouPecaA);
		
		PegouPecaB = new JTextField();
		PegouPecaB.setText(fc.getPegouPecaB());
		PegouPecaB.setBounds(257, 275, 214, 20);
		frmCarrinho.getContentPane().add(PegouPecaB);
		
		JLabel lblPegouPecaB = new JLabel("Pegou pe\u00E7a B?");
		lblPegouPecaB.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPegouPecaB.setBounds(257, 250, 185, 14);
		frmCarrinho.getContentPane().add(lblPegouPecaB);
	}
}
