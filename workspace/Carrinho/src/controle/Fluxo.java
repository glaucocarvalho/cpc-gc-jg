package controle;

import javax.swing.JTextField;

import java.lang.*;  // TO compare strings

import modelo.*;
import rede.*;

public class Fluxo extends Thread {

	// Project OBJ //
	Modelo M = new Modelo();
	
	// JTextFields //
	JTextField textoMsgRec;
	JTextField textoMsgEnv;
	JTextField textoEstadoAtual;
	String msg = "nehuma";
	String lastEstado = "";
	
	// Server //
	ServidorCB serverCB = null;

	// Booleans //
	boolean pegouPecaA = false;
	boolean pegouPecaB = false;
	
	// Getters and setters //	
	public ServidorCB getServerCB() {
		return serverCB;
	}

	public void setServerCB(ServidorCB serverCB) {
		this.serverCB = serverCB;
	}
	
	public JTextField getTextoMsgRec() {
		return textoMsgRec;
	}

	public void setTextoMsgRec(JTextField textoMsgRec) {
		this.textoMsgRec = textoMsgRec;
	}

	public JTextField getTextoMsgEnv() {
		return textoMsgEnv;
	}

	public void setTextoMsgEnv(JTextField textoMsgEnv) {
		this.textoMsgEnv = textoMsgEnv;
	}

	public JTextField getTextoEstadoAtual() {
		return textoEstadoAtual;
	}

	public void setTextoEstadoAtual(JTextField textoEstadoAtual) {
		this.textoEstadoAtual = textoEstadoAtual;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getLastEstado() {
		return lastEstado;
	}

	public void setLastEstado(String lastEstado) {
		this.lastEstado = lastEstado;
	}

	public boolean isPegouPecaA() {
		return pegouPecaA;
	}

	public void setPegouPecaA(boolean pegouPecaA) {
		this.pegouPecaA = pegouPecaA;
	}

	public boolean isPegouPecaB() {
		return pegouPecaB;
	}

	public void setPegouPecaB(boolean pegouPecaB) {
		this.pegouPecaB = pegouPecaB;
	}	
	
	public String getPegouPecaA() {
		String ret = "";
		if (M.isPegouPecaA()){
			ret = "Peguei A Peca A";
		}
		if (!M.isPegouPecaA()){
			ret = "N�O Peguei A Peca A";
		}
		return ret;
	}	
	
	public String getPegouPecaB() {
		String ret = "";
		if (M.isPegouPecaB()){
			ret = "Peguei A Peca B";
		}
		if (!M.isPegouPecaB()){
			ret = "N�O Peguei A Peca B";
		}
		return ret;
	}
	
	// "main" //
	public void acionar ()
	{		
		boolean fim = false;
		while (!fim)
		{
			M.executar();                       		    				
			if (M.isTransmitiu())
			{
				serverCB.enviarMensagem(M.getMensagem());
				//textoMsgEnv.setText(M.getMensagem());
				System.out.println(M.getMensagem());
				M.setTransmitiu(false);
				M.setMensagem("nenhuma");
			}
			if (serverCB.isNovaMsg() == true)
			{
				M.setMensagem(serverCB.getTextoDeEntrada());
				serverCB.setNovaMsg(false);
			}
			//else CO.setMensagem("nenhuma");
			try 
			{
				Thread.sleep(500);
			}
			catch (InterruptedException e) {}
		}	
	}
	
	public void run ()
	{
		acionar();
	}	
}
